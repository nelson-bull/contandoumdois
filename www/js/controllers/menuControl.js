angular.module('starter.controllers')

.controller('menuControl', function($ionicPlatform, $scope, $state) {
  toDebug("menu Control")

  //secao que estamos, se for home eh vazio
  $scope.namePaginaAtual = '';

  //passa o nome da pagina e chama conteudo relativo
  $scope.mudaPagina = function (){
    var _this = event.target, // botao clicado
        $itensMenu = $('.menu-list .item-page > button'), //botoes menu
        $paginas = $('.content-page > *'), //secoes
        paginaNome = _this.getAttribute('data-page'), //data-page dos botoes
        $duvidasAll = $('#doubt-page > section'), // todas as duvidas
        $duvidasHome = $('#doubt-page > section[data-duvida = home]'), //duvidas home
        $duvidasAtual = $('#doubt-page > section[data-duvida = '+ paginaNome +']'); // duvida da secao que estamos


    if($scope.namePaginaAtual !== paginaNome){ //se ja nao estiver clicado
      $scope.namePaginaAtual = paginaNome;
      var $paginaVem = $('.content-page > section[data-page = '+ paginaNome +']'); //define qual pagina abrira

      $itensMenu.removeClass('ativo'); //remove classe dos botoes
      _this.className = 'ativo'; //adiciona classe ao botao clicado

      $paginas.fadeOut(); //some com as paginas
      $duvidasAll.removeClass('ativo');

      $paginaVem.fadeIn(); // mostra a pagina definida

      $duvidasAtual.addClass('ativo');

    }else { //se ja estiver clicado
      $paginas.fadeOut(0);  // some com a pagina
      $duvidasAll.removeClass('ativo');

      $scope.namePaginaAtual = ''; // remove nome da pagina
      $itensMenu.removeClass('ativo'); // remove classe do botao

      $duvidasHome.addClass('ativo');
    }

  }

  //chama pagina de duvidas
  $scope.mostraDuvidas = function () {
    $('#doubt-page').fadeIn(); // mostra as duvidas
  }

  //fecha pagina de duvidas
  $scope.hideDuvidas = function () {
    $('#doubt-page').fadeOut(); // mostra as duvidas
  }


})
