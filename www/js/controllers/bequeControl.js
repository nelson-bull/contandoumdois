angular.module('starter.controllers')

.controller('bequeControl', function($ionicPlatform, $scope, $state) {

  toDebug("beque Control")

  var etapa = 0; //define a etapa e seta seu estado inicial
      etapaFim = 60, //define a etapa final 
      imgUrl = 'img/test/giphy-test', //caminho e nome dos arquivos de imagem
      imgFile = '.jpg', //extensao das imagens
      images = [], //array das imagens a serem carregadas
      fraseAcao1 = 'Arrastar pra baixo',
      fraseAcao2 = 'Yeah! Acendeu';

  $scope.acao = fraseAcao1;   

  //carrega as imagens
  preloadImgs = function () {
    for (var i = 0; i < etapaFim + 1; i++) {
      images[i] = new Image();
      images[i].src = imgUrl + i + imgFile;
      console.log('images loading: ', images[i].src)

    }

    //apos carregar as imagens, tira a tela de loading
    setTimeout(function(){
      $('#loading').removeClass('notReady').fadeOut();
    }, 400)

    return console.log('cabou ibagens!')
  }
  preloadImgs()

  //muda o numero da imagem
  mudaImg = function (novaImagem) {
    var $gifImg = $('.gifImg');
    $gifImg.attr('src', imgUrl + novaImagem + imgFile);

    regraDeTres = (novaImagem * 100) / etapaFim ;
    $('#bequeContainer hr').css('width', regraDeTres + '%')
  }

  finaliza = function (status) {
    if(status){
      $scope.acao = fraseAcao2;
      $('#bequeContainer').addClass('finalized');
      // console.log('acendeu!!!');

    }else {
      $scope.acao = fraseAcao1;
      $('#bequeContainer').removeClass('finalized');
      // console.log('acendeu naooo!');

    }
  }

  //muda imagem pra + no dragdown
  $scope.dragDown = function() {
    if(etapa < etapaFim){
      etapa++ ;
      mudaImg(etapa);
      // console.log('bolando um, etapa: ', etapa)

    }else if(etapa === etapaFim){

      etapa = etapaFim + 1
      /*TO DO -> acabou contar como mais um*/
        //alert('capoooou! =D');
        finaliza(true);
      /*TO DO -> acabou contar como mais um*/

    }else{
      return null
    }
  }

  //muda imagem pra - no dragdown
  $scope.dragUp = function() {
    if(etapa > 0 &&  etapa < etapaFim){
      etapa-- ;
      mudaImg(etapa);
      console.log('desbolando um, etapa: ', etapa)

    }else {
      return null
    }
  }

  //pausa as mudanca de imagem
  $scope.dragEnd = function() {
    if(etapa === etapaFim + 1){
      //alert('capoooou! =D');
      return console.log('ja ta aceso, tio')
    }

    /*descomentar para voltar ao 0 ao soltar o dedo*/
      //etapa = 0;
      //mudaImg(etapa);
    /*descomentar para voltar ao 0 ao soltar o dedo*/

    console.log('parou! etapa ', etapa)
  }

  //reinicia o processo de drag
  $scope.restart = function() {
    etapa = 0;

    /*TO DO -> vamos fazer mais um*/
      //alert('+1');
      finaliza(false);
      mudaImg(etapa);
      console.log('mais um entao!');
    /*TO DO -> vamos fazer mais um*/  
  }

});
